# Site de voyage

permet de visualiser une selection de voyage et payer avec une extension paypal pour wordpress en choisissant les options pour le voyage voulu

## Tech

- Wordpress
- Extension wordpress
- CSS

## Links

- [Git](https://gitlab.com/tipouf/wordpress)

## Authors

- [Tipouf](https://gitlab.com/tipouf)

