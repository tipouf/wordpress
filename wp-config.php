<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wp_tp_eni' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'D&|{=Gh2YD{5Q9tjx^wFR(Gu^/ydqSJf{x+WV$yu,dD~ph-C;/ijx3/dxDr4I:rt' );
define( 'SECURE_AUTH_KEY',  '(m:qEp]{eK!dp0A-,|S`Za-X~3i4gSMys<*fI@Z-BkO>|B4/[Y]4&UO)gzh4*AbI' );
define( 'LOGGED_IN_KEY',    'D1 ip@iIqa@a{S]gv>QlRX9cVut)_Rl4C8p;t(;cBA|}OvK6]jZ^&f|mHEMfrX8Q' );
define( 'NONCE_KEY',        'bCrI5oCZmy_da2TC(/mTvZg/-#pdWUNrGE%PNG3v0Xm_DdM/N,c]bF3 {_GAJ*kt' );
define( 'AUTH_SALT',        'DJT.j*5E(fR=ACU*ISl1TXc.*YMh%UD`Oa|MAU05@zAR5~4EowHGRZ%,xlb,qX*3' );
define( 'SECURE_AUTH_SALT', 'mIumOXn1N#qogQf,PX}1>10u$@$LfAk0PzTnLZnZo4gi)8/$[ ~]A{QM- jT5@`1' );
define( 'LOGGED_IN_SALT',   ']<~cKh=/ixILp?O}rO?_XjP:MNh&t-O-RjT~I[`=LXcEM3f(VwK%iG{ 4T-%:F?f' );
define( 'NONCE_SALT',       '@)Q<mY21S;abed:cJ&k<P6I7Rj3A%r>-Fb]@d|Yb6qlndDH%cK#`ufp;7RKa2o)T' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
